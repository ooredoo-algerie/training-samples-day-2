package live.sample.jira.jql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import com.atlassian.jira.user.ApplicationUser;

import java.util.LinkedList;
import java.util.List;

/**
 * Echoes the the string passed in as an argument.
 */
public class ProjectsCurrentUserAdmin extends AbstractJqlFunction{
	
    private static final Logger log = LoggerFactory.getLogger(ProjectsCurrentUserAdmin.class);
    
    // Our JQL function will not take any parameter when user so to set to 0 or change to the exact number
    public MessageSet validate(ApplicationUser searcher, FunctionOperand operand, TerminalClause terminalClause)
    {
        return validateNumberOfArgs(operand, 0);
    }

    
    // The function returns a list of QueryLiterals that represent the list of IDs of projects where current user is Lead, 
    // and populates a linked list with the results converted to QueryLiterals. 
    
    public List<QueryLiteral> getValues(QueryCreationContext queryCreationContext, FunctionOperand operand, TerminalClause terminalClause)
    {
    	final List<QueryLiteral> literals = new LinkedList<>();
    	 
    	// Getting all projects with a specific property
    	ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    	ProjectManager projectManager = ComponentAccessor.getComponent(ProjectManager.class);
    	
    	List <Project> projectsCurrentUserIsLead = projectManager.getProjectsLeadBy(user);
    	
    	 for (final Project project : projectsCurrentUserIsLead) {
    	        final Long value = project.getId();
    	        log.debug(" ***** Project Found is : " + value );
    	        System.out.println("Project Found is : " + value);
    	
    	        try {
    	            literals.add(new QueryLiteral(operand,value));
    	        } catch (NumberFormatException e) {
    	            log.warn(String.format("List returned a non numeric project IS '%s'.", value));
    	        }
    	    }        
    	    return literals;
    }

    public int getMinimumNumberOfExpectedArguments()
    {
        return 1;
    }

    // In getDataType(), change the data type returned from TEXT to PROJECT, because we return only a list of projects.
    
    public JiraDataType getDataType()
    {
        return JiraDataTypes.PROJECT;
    }

}