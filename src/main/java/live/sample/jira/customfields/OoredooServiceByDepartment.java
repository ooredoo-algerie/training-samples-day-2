package live.sample.jira.customfields;

import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.AbstractCustomFieldType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

@Scanned
public class OoredooServiceByDepartment extends AbstractCustomFieldType<Map<String,Option>,Option> {
    private static final Logger log = LoggerFactory.getLogger(OoredooServiceByDepartment.class);

    // Imports to use
    @ComponentImport private final CustomFieldValuePersister customFieldValuePersister;
    @ComponentImport private final GenericConfigManager genericConfigManager;
    
    public OoredooServiceByDepartment(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager) {
    	super();
    	this.customFieldValuePersister = customFieldValuePersister;
		this.genericConfigManager = genericConfigManager;
    }
    
    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue,
                                                     final CustomField field,
                                                     final FieldLayoutItem fieldLayoutItem) {
       
    	
    	final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);

        // This method is also called to get the default value, in
        // which case issue is null so we can't use it to add currencyLocale
        if (issue == null) {
            return map;
        }

         FieldConfig fieldConfig = field.getRelevantConfig(issue);
         //add what you need to the map here

        return map;
    }

	@Override
	public void createValue(CustomField arg0, Issue arg1, Map<String, Option> arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getChangelogValue(CustomField arg0, Map<String, Option> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Option> getDefaultValue(FieldConfig arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Option getSingularObjectFromString(String arg0) throws FieldValidationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getStringFromSingularObject(Option arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getStringValueFromCustomFieldParams(CustomFieldParams arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Option> getValueFromCustomFieldParams(CustomFieldParams arg0) throws FieldValidationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Option> getValueFromIssue(CustomField arg0, Issue arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Long> remove(CustomField arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDefaultValue(FieldConfig arg0, Map<String, Option> arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateValue(CustomField arg0, Issue arg1, Map<String, Option> arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateFromParams(CustomFieldParams arg0, ErrorCollection arg1, FieldConfig arg2) {
		// TODO Auto-generated method stub
		
	}
}