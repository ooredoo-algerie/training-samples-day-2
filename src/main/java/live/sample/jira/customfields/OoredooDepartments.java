package live.sample.jira.customfields;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.AbstractSingleFieldType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;


 // Annotation to scan class and inject content automatically
 // Extend classes https://docs.atlassian.com/software/jira/docs/api/8.15.1/com/atlassian/jira/issue/customfields/impl/AbstractCustomFieldType.html
 // Notice also that we'll use sTRING as our 'transport object' (for dealing with a currency in Java). A transport object is just a plain old Java object (POJO). The object type represents the custom field used


@Scanned
public class OoredooDepartments extends AbstractSingleFieldType<String> {
    private static final Logger log = LoggerFactory.getLogger(OoredooDepartments.class);

    // Imports to use
    @ComponentImport private final CustomFieldValuePersister customFieldValuePersister;
    @ComponentImport private final GenericConfigManager genericConfigManager;
    
    public OoredooDepartments(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager) {
    	super(customFieldValuePersister, genericConfigManager);
    	this.customFieldValuePersister = customFieldValuePersister;
		this.genericConfigManager = genericConfigManager;
    }
    
    
    // Our field will use the default implementation from the AbstractCustomFieldType superclass
    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue,
                                                     final CustomField field,
                                                     final FieldLayoutItem fieldLayoutItem) {
        final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);

        // This method is also called to get the default value, in
        // which case issue is null so we can't use it to add currencyLocale
        if (issue == null) {
            return map;
        }

         FieldConfig fieldConfig = field.getRelevantConfig(issue);
         //add what you need to the map here

        return map;
    }

    // This method turns a value in our Transport object into text.
    @Override
	public String getStringFromSingularObject(String code) {
		if(code == null)
			return null;
		else
			return code;
	}
    
    // The method takes input from the user, validates it, and then puts it into a transport object. We want to validate that the user enters 4 Letters code set to upper case.
    @Override
	public String getSingularObjectFromString(String code) throws FieldValidationException {
		if(code == null) {
			return null;
		}else {
			if(isStringUpperCase(code) && code.length() == 4) {
				log.warn("Tests" + code);
				return code;
			}else {
				 throw new FieldValidationException("All Ooredoo codes must be set to Upper Case and contains 4 letters!");
			}
		}
			
	}

    //To tell Jira in what kind of database column to store the data, add the getDatabaseType() method. You can choose text, long text, numeric, or date. We could use numeric, but we will use text to keep it simple.
	@Override
	protected PersistenceFieldType getDatabaseType() {
		  return PersistenceFieldType.TYPE_LIMITED_TEXT;
	}

	// This method takes a value as our transport object and converts it to an Object suitable for storing in the database. In our case we want to convert to String.
	@Override
	protected String getDbValueFromObject(String codeObject) {
		   return getStringFromSingularObject(codeObject);
	}

	// This takes a value from the database and converts it to transport object. The value parameter is declared as Object, but will be String, Double, or Date depending on the database type defined above. Because we chose FieldType TEXT, we will get a String and can reuse getSingularObjectFromString()
	@Override
	protected String getObjectFromDbValue(Object databasecodevalue) throws FieldValidationException {
		 return getSingularObjectFromString((String) databasecodevalue);
	}
	
   private static boolean isStringUpperCase(String str){
        
        //convert String to char array
        char[] charArray = str.toCharArray();
        
        for(int i=0; i < charArray.length; i++){
            
            //if any character is not in upper case, return false
            if( !Character.isUpperCase( charArray[i] ))
                return false;
        }
        
        return true;
    }
}