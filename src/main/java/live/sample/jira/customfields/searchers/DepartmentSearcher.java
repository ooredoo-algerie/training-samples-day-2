package live.sample.jira.customfields.searchers;

import com.atlassian.jira.issue.customfields.searchers.TextSearcher;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

@Scanned
public class DepartmentSearcher extends TextSearcher{
    @ComponentImport private final FieldVisibilityManager fieldVisibilityManager;
    @ComponentImport private final JqlOperandResolver jqlOperandResolver;
    @ComponentImport private final CustomFieldInputHelper customFieldInputHelper;
    
    
	public DepartmentSearcher(FieldVisibilityManager fieldVisibilityManager,JqlOperandResolver jqlOperandResolver,CustomFieldInputHelper customFieldInputHelper) {
		super(fieldVisibilityManager, jqlOperandResolver, customFieldInputHelper);
		this.fieldVisibilityManager = fieldVisibilityManager;
		this.jqlOperandResolver = jqlOperandResolver;
		this.customFieldInputHelper = customFieldInputHelper;
	}
}
